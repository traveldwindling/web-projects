## Purpose

A basic template for Web projects.

## `normalize.css`

[normalize.css](https://github.com/necolas/normalize.css) by [necolas](https://github.com/necolas) is licensed under a [MIT](https://github.com/necolas/normalize.css/blob/master/LICENSE.md) license.