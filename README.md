# Web Projects

This repository contains a collection of custom Web projects.

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="web_template">Web Template</a></td>
      <td>A basic template for Web projects.</td>
    </tr>
  </tbody>
</table>

## Project Avatar

`logo.png` is [Logo Sitio Web.png](https://commons.wikimedia.org/wiki/File:Logo_Sitio_Web.png) by kisspng and is licensed under a [Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.

## `normalize.css`

[`normalize.css`](https://github.com/necolas/normalize.css) by Nicolas Gallagher and Jonathan Neal is licensed under the [MIT](https://github.com/necolas/normalize.css/blob/master/LICENSE.md) license.